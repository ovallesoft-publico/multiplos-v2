const {Schema, model} = require('mongoose');

const multiploSchema = new Schema({
    name: {
      type:  String
    },
    multiple: {
        type:  Number
      },
});


module.exports = model('multiplo', multiploSchema);