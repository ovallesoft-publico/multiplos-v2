const {Router} = require('express');
const router = Router();

const {getMultiplo} = require('../controllers/multiploController');

router.route('/').get(getMultiplo);

module.exports = router;
