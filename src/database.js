const mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;

var url = "mongodb://localhost:27017/";

MongoClient.connect(url,{
    useUnifiedTopology:true,
    useNewUrlParser:true
}, 
function(err, db) {

    if (err) throw console.log(err);

    var dbo = db.db("db-challenge");

    console.log("db-challenge bases de creada!");

    dbo.listCollections({name: "multiplos"})
    .next(function(err, collinfo) {
        if (!collinfo) {

            dbo.createCollection("multiplos", function(err, res) {
        
                if (err) throw console.log(err);
                console.log("Tabla creada!");
        
                db.close();
        
            });
        
            var registros = [{ name: "Música", multiple: 3 },
                             { name: "TI", multiple: 5 },
                             { name: "Musical", multiple: 0 }];
        
            
            dbo.collection("multiplos").insertMany(registros, function(err, res) {
                
                if (err) throw console.log(err);
        
              console.log("3 documentos insertedos");
              
              db.close();
        
            });
           
        }
    });

  });

  const URI = process.env.MONGODB_URI ? process.env.MONGODB_URI : 'mongodb://localhost:27017/db-challenge';


mongoose.connect(URI, {
    useUnifiedTopology:true,
    useNewUrlParser:true,
    useCreateIndex:true
});

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('Se ha conecatdo la BD con exito!');
})