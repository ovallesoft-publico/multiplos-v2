const multiplocontroller = {};

const multiploModel = require('../models/multiplo');

multiplocontroller.getMultiplo = async (req, res) => {

    const multiplo = await multiploModel.find();
    let musica = multiplo[0].name;
    let ti = multiplo[1].name;
    let musical = multiplo[2].name;
    let multipleTres = multiplo[0].multiple;
    let multipleCinco = multiplo[1].multiple;

    const arreglo = [];

    for(let j = 1; j<= 100; j++){

        switch (true) {
            case j % multipleTres == 0 && j % multipleCinco == 0:
                arreglo.push(musical);
            break;
            case j % multipleTres == 0:
                arreglo.push(musica);
            break;
            case j % multipleCinco == 0:
                arreglo.push(ti);
            break;
            default:
            arreglo.push(j);
        }

    }

    res.json(arreglo); 

};

module.exports = multiplocontroller;