const express = require('express');
const app = express();
const cors = require('cors');

//settings
app.set('port', process.env.PORT_WEB || 3000);

//middlwares
app.use(cors());
app.use(express.json());


//routes
app.use('/api/multiplos', require('./routes/multiplo'));

module.exports = app;